/**
 * Created by Jakub on 2017-05-03.
 */

import React from "react"
import Note from "./Note"
import NoteForm from "./NoteForm"
import notes from "../constant/notes"

export default class Board extends React.Component{
    constructor(arg){
        super(arg)
        this.state = {
            notes: notes,
            maxNotesId: 5,
            noteFormShow: false,
            formData: {
                formTittle: "New Sticky Note",
            }
        }
        this.setNoteFormVisible = this.setNoteFormVisible.bind(this)
        this.addNewNote = this.addNewNote.bind(this)
        this.deleteNote = this.deleteNote.bind(this)
        this.editNote = this.editNote.bind(this)
        this.updateNote = this.updateNote.bind(this)
    }


    addNewNote(note){
        this.setState({
            notes: [...this.state.notes, note],
            maxNotesId: this.state.maxNotesId + 1
        })
    }

    deleteNote(id){
        var notes = this.state.notes
        this.setState({ notes: notes.filter(note => note.id !== id)})
    }

    editNote(id){
        let note = this.state.notes.find(element => element.id === id)
        if (note) {
            this.setState({
                noteFormShow: true,
                formData: {
                    formTittle: "Edit Note",
                    note: note
                }
            })
        }
    }

    updateNote(note){
        var notesList = this.state.notes
        let noteElement = {...notesList.find(element => element.id === note.id), ...note}
        notesList = notesList.filter(element => element.id !== note.id )
        notesList = [...notesList, noteElement]
        notesList.sort(function(a, b) {
            return a.id - b.id
        });
        this.setState({
            notes: notesList
        })

    }

    setNoteFormVisible(isVisible){
        this.setState({
            noteFormShow: isVisible,
            formData: {
                formTittle: "New Sticky Note",
            }

        })
    }

    renderNotesList(notesList){
        return(
            <div className="notes-container">
                { notesList.map((note) => <Note key={note.id} note={note} deleteNote={this.deleteNote}
                                                            editNote={this.editNote}/>) }
                <div className="note">
                    <div className="top-bar">
                    </div>
                    <div className="note-add">
                        <i className="fa  fa-plus" onClick={() => this.setNoteFormVisible(true)} />
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="board">
                { this.renderNotesList(this.state.notes) }
                    { this.state.noteFormShow ? <NoteForm formData={this.state.formData}
                                                          setNoteFormVisible={this.setNoteFormVisible}
                                                          addNewNote={this.addNewNote}
                                                          maxNotesId={this.state.maxNotesId}
                                                          updateNote={this.updateNote}/> : ""}
            </div>
        )
    }
}