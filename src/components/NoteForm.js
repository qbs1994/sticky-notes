import React from 'react'
import faIconsSelect from '../constant/faIconsSelect'

export default class NoteForm extends React.Component{
    constructor(arg){
        super(arg)
        this.animationTime = 0.9
        this.state = {
            animationClass: "form-in",
            fieldTittle: "",
            fieldText: "",
            fieldColor: 0,
            icon: "fa-sticky-note",
            formErrors: {
                text: false,
                tittle: false,
            }
        }
        setTimeout(() => this.setState({animationClass: ""}), this.animationTime * 1000)

        this.hideForm = this.hideForm.bind(this)
        this.createNote = this.createNote.bind(this)
        this.handleFormEdit = this.handleFormEdit.bind(this)
        this.validateValues = this.validateValues.bind(this)
    }

    componentDidMount(){
        if(this.props.formData.note){
            this.setState({
                fieldText: this.props.formData.note.text,
                fieldTittle: this.props.formData.note.tittle,
                fieldColor: this.props.formData.note.color,
                icon: this.props.formData.note.icon,
            })
        }
    }


    hideForm(){
        this.setState({animationClass: "form-out"})
        setTimeout(() => this.props.setNoteFormVisible(false), this.animationTime * 1000)
    }

    handleFormEdit(field, value){
        this.validateValues(field, value)
        if (field === "tittle") {
            this.setState({fieldTittle: value})
            this.validateValues(value, this.state.fieldText)
        }
        if (field === "text") {
            this.setState({fieldText: value})
            this.validateValues(this.state.fieldTittle, value)
        }
    }

    validateValues(tittle, text){
        let errors = this.state.formErrors
        if (tittle.length < 3){
            errors.tittle = "Tittle too short"
        }
        else if(tittle.length > 25){
            errors.tittle = "Tittle too long"
        }
        else{
            errors.tittle = false
        }

        if (text.length > 300) {
            errors.text = "Text too long"
        }
        else {
                errors.text = false
        }
        this.setState({formErrors: errors})
        if (!errors.tittle && !errors.text)
            return true
        else
            return false
    }


    createNote() {
        if (this.validateValues(this.state.fieldTittle, this.state.fieldText)){
            if (this.props.formData.note) {
                this.props.updateNote(
                    {
                        id: this.props.formData.note.id,
                        tittle: this.state.fieldTittle,
                        text: this.state.fieldText,
                        color: this.state.fieldColor,
                        icon: this.state.icon,
                    }
                )
            }
            else {
                this.props.addNewNote(
                    {
                        id: this.props.maxNotesId + 1,
                        tittle: this.state.fieldTittle,
                        text: this.state.fieldText,
                        color: this.state.fieldColor,
                        icon: this.state.icon,
                    }
                )
            }
            this.hideForm()
        }
    }

    render(){
        return(
            <div className={"cover-content cover-"  + this.state.animationClass}>
                <div className={"background-" + this.state.fieldColor + " note-form " + this.state.animationClass}>
                    <div className="top-bar">
                        <div className="text-left">
                        </div>
                        <div className="text-center">
                            <h1>{this.props.formData.formTittle}</h1>
                        </div>
                        <div className="text-right">
                            <i className="fa fa-times-circle-o" onClick={this.hideForm}/>
                        </div>
                    </div>
                    <div className="content">
                        <div>
                            <div className="form-group">
                                <div className="color-picker">
                                    <div  className="background-0" data-select={this.state.fieldColor === 0} onClick={() => this.setState({fieldColor: 0})}></div>
                                    <div  className="background-1" data-select={this.state.fieldColor === 1} onClick={() => this.setState({fieldColor: 1})}></div>
                                    <div  className="background-2" data-select={this.state.fieldColor === 2} onClick={() => this.setState({fieldColor: 2})}></div>
                                    <div  className="background-3" data-select={this.state.fieldColor === 3} onClick={() => this.setState({fieldColor: 3})}></div>
                                    <div  className="background-4" data-select={this.state.fieldColor === 4} onClick={() => this.setState({fieldColor: 4})}></div>
                                </div>
                                <br/>
                                <label>Tittle</label>
                                <input type="text"  className={this.state.formErrors.tittle ? "form-control form-error" : "form-control"} value={this.state.fieldTittle}  onChange={(event) => this.handleFormEdit("tittle", event.target.value)}/>
                                {this.state.formErrors.tittle ? (<p className="form-error-message"> <i className="fa fa-exclamation-circle"/> {this.state.formErrors.tittle} <i className="fa fa-exclamation-circle"/> </p>) : ""}
                            </div>
                            <div className="form-group">
                                <label>Note</label>
                                <textarea className={ this.state.formErrors.text ? "form-control form-error" : "form-control"} rows="8" value={this.state.fieldText} onChange={(event) => this.handleFormEdit("text", event.target.value)}/>
                                {this.state.formErrors.text ? (<p className="form-error-message"> <i className="fa fa-exclamation-circle"/> {this.state.formErrors.text} <i className="fa fa-exclamation-circle"/> </p>) : ""}
                            </div>
                            <div className="icon-picker">
                                {faIconsSelect.map((icon) =>
                                    <i key={icon} className={"fa " + icon + (this.state.icon === icon ? " select" : " ")} onClick={() => this.setState({icon: icon})}/>
                                )}
                            </div>
                            <br/>
                           <div className="submit text-center"><i className="fa fa-check-circle-o" onClick={this.createNote}/></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}