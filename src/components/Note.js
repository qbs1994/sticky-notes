/**
 * Created by Jakub on 2017-05-05.
 */

import React from 'react'

export default class Note extends React.Component{
    constructor(arg){
        super(arg)
        this.flipTime = 0.6
        this.state = {
            side: true,
            containerFlipClass: ""
        }
        this.flip = this.flip.bind(this)
        this.edit = this.edit.bind(this)
    }

    flip(direction){
        if(direction){
            this.setState({
                containerFlipClass: "flipOut"
            })
            setTimeout(() => this.setState({containerFlipClass: ""}), this.flipTime * 1000 )
            setTimeout(() => this.setState({side: true}), this.flipTime * 1000/2 )
        }
        else{
            this.setState({
                containerFlipClass: "flipOut"
            })
            setTimeout(() => this.setState({containerFlipClass: ""}), this.flipTime * 1000 )
            setTimeout(() => this.setState({side: false}), this.flipTime * 1000/2 )
        }
    }

    edit(){
        this.props.editNote(this.props.note.id)
    }

    render(){
        return(
            <div className={this.state.containerFlipClass + " note background-" + this.props.note.color }>
                <div className={!this.state.side ? "top-bar hide" : "top-bar"} >
                    <div className="text-left">
                        <i className="fa fa-pencil" onClick={this.edit}/>
                    </div>
                    <div className="text-center">
                        <i className="fa fa-share" onClick={() => this.flip(false)}/>
                    </div>
                    <div className="text-right">
                        <i className="fa fa-trash" onClick={() => this.props.deleteNote(this.props.note.id)} />
                    </div>
                </div>
                <div className={!this.state.side ? "note-content hide" : "note-content"}>

                    <h1><i className={"fa " + this.props.note.icon}/> <br/><br/> {this.props.note.tittle}</h1>
                    <p>{this.props.note.text}</p>
                </div>

                <div className={this.state.side ? "top-bar hide" : "top-bar"} >
                    <div className="text-left">
                    </div>
                    <div className="text-center">
                        <i className="fa fa-share" onClick={() => this.flip(true)}/>
                    </div>
                    <div className="text-right">
                    </div>
                </div>
                <div className={this.state.side ? "note-content hide" : "note-content"}>

                </div>
            </div>

    )
    }
}