import React, { Component } from 'react';
import './style/style.css'
import Board from './components/Board'


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
            <div className="tittle">
                <hr className="left"/><span className="text">Sticky Notes beta</span><hr className="right" />
            </div>
        </div>
        <Board/>

      </div>
    );
  }
}

export default App;
