/**
 * Created by Jakub on 2017-05-16.
 */
export default [
    {
        id: 0,
        tittle: "Visit university",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        color: "1",
        icon: "fa-graduation-cap "
    },    {
        id: 1,
        tittle: "Celebrate birthday",
        text: "Nam libero eros, aliquet in dignissim non, mollis a eros. Nulla pulvinar sodales tellus, et finibus quam condimentum eu. Nullam condimentum viverra orci, sed vestibulum magna tempus quis. Curabitur finibus congue libero, id sollicitudin orci suscipit quis. ",
        color: "3",
        icon: "fa-glass "
    },    {
        id: 2,
        tittle: "Print documents",
        text: "Nunc eu ex id metus dapibus efficitur condimentum eu erat. Aenean venenatis bibendum nulla a egestas. Nulla cursus venenatis eleifend.",
        color: "1",
        icon: "fa-print "
    },    {
        id: 3,
        tittle: "Develop new apps",
        text: "Eniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        color: "0",
        icon: "fa-magic "
    },    {
        id: 4,
        tittle: "Charge phone",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        color: "2",
        icon: "fa-battery-full "
    },    {
        id: 5,
        tittle: "Buy new car",
        text: "In sit amet nibh facilisis, blandit enim feugiat, imperdiet ipsum. Fusce non est ac urna placerat maximus in ac orci. Morbi interdum ex sit amet nibh suscipit congue et quis sapien. In id mi congue, blandit dolor sed, aliquet erat. Nunc vel mauris dapibus nibh aliquam dapibus quis a lorem. ",
        color: "4",
        icon: "fa-car "
    },
]
