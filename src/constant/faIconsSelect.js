/**
 * Created by Jakub on 2017-05-12.
 */
export default  [
    "fa-sticky-note",
    "fa-address-book",
    "fa-area-chart",
    "fa-automobile",
    "fa-bank",
    "fa-battery-full",
    "fa-bell-o",
    "fa-calendar",
    "fa-child",
    "fa-glass",
    "fa-graduation-cap",
    "fa-group",
    "fa-magic",
    "fa-print",
    "fa-check",
    "fa-times",
    "fa-minus",
    "fa-exclamation",
]